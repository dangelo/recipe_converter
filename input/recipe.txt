# Salade de pâtes à l'italienne

## Ingrédients
* 300 g de pâtes
* 1 melon
* 4 belles tomates
* 150 g de mozzarella ou de feta
* 4 tranches de jambon de parme
* 12 feuilles de basilic
* 8 olives noires dénoyautées
* Le jus d'1 citron
* 6 cuillères à soupe d'huile d'olive
* Sel, poivre

## Infos
4 personnes
Préparation : 20 min

## Étapes
* Couper le melon en deux et détailler la chair en billes à l'aide d'une cuillère
parisienne. Couper les tomates en morceaux, la mozzarella en dés et le jambon en lanières. Détailler les olives en petits morceaux.
* Dans un saladier, mélanger le jus d'un citron, l'huile d'olive et 6 feuilles de basilic
ciselées. Saler et poivrer.
* Ajouter les pâtes dans le saladier. Mélanger. Ajouter les billes de melon, les tomates,
la mozzarella, le jambon et les olives. Parsemer du reste de basilic.

## Conseils
Servir frais, le melon n'est pas indispensable.