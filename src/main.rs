extern crate chrono;
extern crate clap;
extern crate tex_gen;
extern crate walkdir;

use std::{fs, io};
use std::fs::{File, DirEntry, ReadDir, OpenOptions};
use std::io::{BufRead, BufReader, Write};
use std::path::Path;
use std::path::PathBuf;
use walkdir::WalkDir;

use chrono::prelude::*;
use clap::{App, Arg};

use crate::recipe_builder::recipe_part::{RecipePart, RecipePartBlock};
use crate::recipe_builder::RecipeBuilder;
use std::collections::HashMap;

mod recipe_builder;

// pour ts

// cargo run  ~/Documents/PROJETS/LivreCuisine/recettes_src/ ~/Documents/PROJETS/LivreCuisine/recettes/
// cargo test -- --nocapture

// TODO TARGET
// TODO dans main.rs -> lecture arborescence fichiers et read one line (fs::read_to_string)
// TODO convert_to_tex
// TODO dans recipe_mapper -> mapper le fichier à une struct
// TODO dans recipe_converter -> générer un tex ? une string ? à partir de la struct
// TODO recipe_locator -> gère ou on veut la recette ? génère le tex d'include ?

// TODO test with Vec<String> directement dans l'enum Blocktype ?
// TODO passage par builder: manque begin{recette} and end{recette}

// TODO allow to set output path and if not supplied out in input dir (by default for now)
// TODO index and list
// TODO not writing empty section
// TODO be able write by section to allow change in section writing order
// TODO Fail if no title  -> no line with #

fn main() {
    let matches = App::new("recipeConverter")
        .version("1.0")
        .about("Convert md recipe to tex !")
        .author("dangelob")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .help("Sets the output dir to use")
                .required(true)
                .index(2),
        )
        .get_matches();

    let input: String = matches.value_of("INPUT").unwrap().to_string();
    let output: String = matches.value_of("OUTPUT").unwrap().to_string();

    let file_or_dir_input = Path::new(&input);
    let output_dir = Path::new(&output);
    println!("Using Output dir: {:?}", output_dir);

    let mut file_count = 0;

    if file_or_dir_input.exists() && file_or_dir_input.is_file() {
        println!("Using input file: {}", matches.value_of("INPUT").unwrap());
        treat_file(&file_or_dir_input, &output_dir);
        file_count += 1;
    } else if file_or_dir_input.exists() && file_or_dir_input.is_dir() {
        println!("Using input dir: {}", matches.value_of("INPUT").unwrap());

        let mut current_file = PathBuf::new();
        let mut part = "".to_string();


        for entry in walkDir(file_or_dir_input) {
            let entry = entry.unwrap();
            if entry.path().is_dir() {
                println!("dir: {}", entry.path().display());


                part = format!("{}", entry.path().file_name().unwrap().to_os_string().to_str
                ().unwrap());
//                current_file = format!("/tmp/{}.tex", &part);
                current_file = Path::new(&output_dir).join(&part).with_extension("tex");

                let mut f = File::create(&current_file).unwrap();
//                let mut first_line = format!("\\chapitre{{{}}}\n", &part);
//                f.write(first_line.as_bytes()).unwrap();


            } else {
                println!("file: {}", entry.path().display());
                println!("current_file: {}", &current_file.to_str().unwrap());
                //fixme comment détecter qu'on a terminé les fichiers d'un dossier s'il y a des
                // fichier à la racine ? (en gros ça fonctionne quand chaque recette est dans un
                // sous dossier
                let relative_out_dir = get_relative_path_from_indir(&entry.path(), &file_or_dir_input);

                let out_dir = compute_out_dir( relative_out_dir, output_dir);

                treat_file(&entry.path(), out_dir.as_path());

                println!("current_file Try to update: {}", &current_file.to_str().unwrap());
                let mut reci = format!("\\input{{./recettes/{}/{}}}\n", &part, &entry.path()
                    .file_stem()
                    .unwrap().to_str().unwrap());
                let mut f = OpenOptions::new().append(true).open(&current_file).unwrap();
                let _ = f.write_all(reci.as_bytes());
                f.flush().unwrap();


                file_count += 1;
            }
        }
    }
    println!("Written recipes: {}", file_count);
}

fn get_relative_path_from_indir(input_file: &Path, input_dir: &Path) -> PathBuf {
    let path_to_file = input_file.parent().unwrap();
    let filename = input_file.file_stem().unwrap();
    let relative_path_from_input_dir = path_to_file.strip_prefix(input_dir).unwrap();
    relative_path_from_input_dir.join(filename).with_extension("tex")
}

fn compute_out_dir(relative_output_dir: PathBuf, output_dir: &Path) -> PathBuf {
    output_dir.join(relative_output_dir)
}

fn walkDir(file_or_dir_input: &Path) -> WalkDir {
    println!("Reading from : {:?}", file_or_dir_input);
    WalkDir::new(file_or_dir_input)
}


fn treat_file(file_path: &Path, output_dir_path: &Path) {
    let f = BufReader::new(File::open(file_path).unwrap());
    let timestamp: String = format!(
        "{}{}{}",
        "% Generated file ".to_string(),
        Local::now(),
        "\n"
    );
    let mut current_recipe_part: RecipePartBlock = RecipePartBlock::Unknown;

    let mut trim_title: String = "".to_owned();
    let mut ingredients: Vec<String> = vec![];
    let mut etapes: Vec<String> = vec![];
    let mut infos: Vec<String> = vec![];
    let mut conseils: Vec<String> = vec![];

    for line in f.lines() {
//                println!("1 Lecture de la ligne : {:?}", &line);
        match line {
            Ok(line) => {
                if line.starts_with("# ") {
                    trim_title = trim_line(line);
                } else if line.starts_with("##") {
                    current_recipe_part = find_current_recipe_part(trim_line(line));
                } else if !line.trim().is_empty() {
                    match current_recipe_part {
                        RecipePartBlock::Ingredients => ingredients.push(trim_line(line)),
                        RecipePartBlock::Etapes => etapes.push(trim_line(line)),
                        RecipePartBlock::Infos => infos.push(trim_line(line)),
                        RecipePartBlock::Conseils => conseils.push(trim_line(line)),
                        RecipePartBlock::Unknown => (),
                    }
                }
            }
            Err(e) => panic!("Error reading file: {}", e),
        }
    }

    let recipe = RecipeBuilder::new()
        .timestamp(timestamp)
        .name(trim_title)
        .recipe_part(RecipePart {
            btype: RecipePartBlock::Ingredients,
            items: ingredients,
        })
        .recipe_part(RecipePart {
            btype: RecipePartBlock::Infos,
            items: infos,
        })
        .recipe_part(RecipePart {
            btype: RecipePartBlock::Etapes,
            items: etapes,
        })
        .recipe_part(RecipePart {
            btype: RecipePartBlock::Conseils,
            items: conseils,
        })
        .stringify();
    println!("debug - writing: {}", output_dir_path.display());
    fs::create_dir_all(&output_dir_path.parent().unwrap());
    fs::write(output_dir_path, recipe).expect("Unable to write file");
    println!("debug - END writing: {}", output_dir_path.display());
}

fn trim_line(line: String) -> String {
    let chars_to_trim: &[char] = &['#', '*', ' '];
    line.trim_matches(chars_to_trim).to_string()
}

fn find_current_recipe_part(line: String) -> RecipePartBlock {
    match line.as_ref() {
        "Ingrédients" => RecipePartBlock::Ingredients,
        "Étapes" => RecipePartBlock::Etapes,
        "Infos" => RecipePartBlock::Infos,
        "Conseils" => RecipePartBlock::Conseils,
        _ => RecipePartBlock::Unknown,
    }
}

#[test]
fn compute_out_dir_test(){
    let outdir = Path::new("/home/titi/output");
    let rel_out = Path::new("cake/filecake1").with_extension("tex");


    let out = compute_out_dir( rel_out,  &outdir );
    assert_eq!(Path::new("/home/titi/output/cake/filecake1.tex"), out);

}
#[test]
fn get_relative_path_from_indir_test(){
    let infile = Path::new("/home/titi/input/cake/filecake1.md");
    let indir = Path::new("/home/titi/input");


    let out = get_relative_path_from_indir( &infile, &indir );
    assert_eq!(Path::new("cake/filecake1.tex"), out);

}

#[test] // Integ test
fn treat_file_test() {
    let input_file = Path::new("input/recipe.txt");
    let output_dir = Path::new("/tmp/recipe.tex");
    treat_file(&input_file, &output_dir);

//    let file_path = &output_dir.join("recipe").with_extension("tex");
    let f = BufReader::new(File::open(&output_dir).unwrap());

    for (i, line) in f.lines().enumerate() {
        println!("line: {} - {:?}", &i, &line);
        match line {
            Ok(line) => match i {
                1 => assert_eq!(
                    "\\begin{recette}{Salade de pâtes à l'italienne}{Salade de pâtes à \
                     l\'italienne}",
                    &line.to_owned()
                ),
                3 => assert_eq!("\\begin{ingredients}", &line.to_owned()),
                5 => assert_eq!("1 melon\\par", &line.to_owned()),
                14 => assert_eq!("\\end{ingredients}", &line.to_owned()),
                15 => assert_eq!("\\begin{infos}", &line.to_owned()),
                17 => assert_eq!("Préparation : 20 min\\\\", &line.to_owned()),
                18 => assert_eq!("\\end{infos}", &line.to_owned()),
                19 => assert_eq!("\\begin{etapes}", &line.to_owned()),
                20 => assert_eq!(
                    "\\item Couper le melon en deux et détailler la chair en \
                     billes à \
                     l\'aide d\'une cuillère",
                    &line.to_owned()
                ),
                26 => assert_eq!("\\end{etapes}", &line.to_owned()),
                30 => assert_eq!("\\end{recette}", &line.to_owned()),
                _ => (),
            },
            Err(e) => panic!("Error reading file: {}", e),
        }
    }
}
