pub(crate) mod recipe_part;

use crate::recipe_builder::recipe_part::{RecipePart, RecipePartBlock};
use tex_gen::{CmdBuilder, TagType};

pub struct RecipeBuilder {
    pub timestamp: String,
    pub recipe_name: String,
    pub ingredients: String,
    pub etapes: String,
    pub infos: String,
    pub conseils: String,
}

impl RecipeBuilder {
    pub fn new() -> RecipeBuilder {
        RecipeBuilder {
            timestamp: "".to_owned(),
            recipe_name: "".to_owned(),
            ingredients: "".to_owned(),
            etapes: "".to_owned(),
            infos: "".to_owned(),
            conseils: "".to_owned(),
        }
    }

    pub fn timestamp(&mut self, timestamp: String) -> &mut RecipeBuilder {
        self.timestamp = timestamp;
        self
    }
    pub fn name(&mut self, name: String) -> &mut RecipeBuilder {
        self.recipe_name = name;
        self
    }

    pub fn recipe_part(&mut self, recipe_part: RecipePart) -> &mut RecipeBuilder {
        match recipe_part.btype {
            RecipePartBlock::Ingredients => self.ingredients = recipe_part.stringify(),
            RecipePartBlock::Etapes => self.etapes = recipe_part.stringify(),
            RecipePartBlock::Infos => self.infos = recipe_part.stringify(),
            RecipePartBlock::Conseils => self.conseils = recipe_part.stringify(),
            RecipePartBlock::Unknown => (),
        }
        self
    }

    pub fn stringify(&self) -> String {
        let begin_block = CmdBuilder::new()
            .tag_type(TagType::Begin)
            .name("recette".to_owned())
            .par1(self.recipe_name.clone())
            .build();

        let end_block = CmdBuilder::new()
            .tag_type(TagType::End)
            .name("recette".to_owned())
            .build();

        format!(
            "{}{}\n{}{}{}{}{}",
            self.timestamp,
            begin_block,
//            self.recipe_name.clone(),
            self.ingredients,
            self.infos,
            self.etapes,
            self.conseils,
            end_block
        )
    }
}

#[test]
fn test_recipe_builder() {
    let recipe = RecipeBuilder::new()
        .name("Ma Recette".to_owned())
        .recipe_part(RecipePart {
            btype: RecipePartBlock::Ingredients,
            items: vec!["A".to_owned(), "B".to_owned()],
        })
        .recipe_part(RecipePart {
            btype: RecipePartBlock::Etapes,
            items: vec!["1".to_owned(), "2".to_owned()],
        })
        .stringify();

    assert_eq!("\\begin{recette}{Ma Recette}{Ma Recette}\n\n\\begin{ingredients}\nA\\par\nB\\par\n\\end{ingredients}\n\\begin{etapes}\n\\item 1\n\\item 2\n\\end{etapes}\n\\end{recette}\n", recipe)
}
