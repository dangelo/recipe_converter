extern crate tex_gen;

use tex_gen::{CmdBuilder, TagType};

pub struct RecipePart {
    pub btype: RecipePartBlock,
    pub items: Vec<String>,
}

impl RecipePart {
    pub fn stringify(&self) -> String {
        let begin_block = CmdBuilder::new()
            .tag_type(TagType::Begin)
            .name(self.btype.as_string())
            .build();

        let end_block = CmdBuilder::new()
            .tag_type(TagType::End)
            .name(self.btype.as_string())
            .build();

        let mut concat_items: Vec<String> = vec![];

        for item in &self.items {
            // TODO to iter
            concat_items.push(self.btype.line_suffix());
            concat_items.push(item.clone());
            concat_items.push(self.btype.line_prefix());
        }

        if self.btype != RecipePartBlock::Unknown {
            format!("{}{}{}", begin_block, concat_items.join(""), end_block)
        } else {
            "".to_owned()
        }
    }
}

#[derive(PartialEq)]
pub enum RecipePartBlock {
    Ingredients,
    Infos,
    Etapes,
    Conseils,
    Unknown,
}

impl RecipePartBlock {
    pub fn as_string(&self) -> String {
        match self {
            RecipePartBlock::Ingredients => "ingredients".to_owned(),
            RecipePartBlock::Etapes => "etapes".to_owned(),
            RecipePartBlock::Infos => "infos".to_owned(),
            RecipePartBlock::Conseils => "conseils".to_owned(),
            RecipePartBlock::Unknown => "Unknown".to_owned(),
        }
    }

    pub fn line_prefix(&self) -> String {
        match self {
            RecipePartBlock::Ingredients => "\\par\n".to_owned(),
            RecipePartBlock::Infos => "\\\\\n".to_owned(),
            _ => "\n".to_owned(),
        }
    }

    pub fn line_suffix(&self) -> String {
        match self {
            RecipePartBlock::Etapes => "\\item ".to_owned(),
            _ => "".to_owned(),
        }
    }
}

#[test]
fn test_recipe_part_builder() {
    let recipe_part = RecipePart {
        btype: RecipePartBlock::Ingredients,
        items: vec!["A".to_owned(), "B".to_owned()],
    }
    .stringify();
    assert_eq!(
        "\\begin{ingredients}\nA\\par\nB\\par\n\\end{ingredients}\n",
        recipe_part
    )
}

#[test]
fn test_recipe_part_builder_unknown() {
    let recipe_part = RecipePart {
        btype: RecipePartBlock::Unknown,
        items: vec!["A".to_owned(), "B".to_owned()],
    }
    .stringify();
    assert_eq!("", recipe_part)
}
